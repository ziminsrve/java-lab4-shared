package lab4.java.frontend;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTreeTableCell;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lab4.java.common.Subtask;
import lab4.java.common.Task;
import lab4.java.common.TaskDao;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

public class MainFrm {

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab tabFilter;

    // Основная вкладка
    @FXML
    private TreeTableView<Subtask> listTasksTreeTableView;

    @FXML
    private TreeTableColumn<Subtask, String> nameColumn;

    @FXML
    private TreeTableColumn<Subtask, String> descriptionColumn;

    @FXML
    private TreeTableColumn<Subtask, String> deadlineColumn;

    @FXML
    private TreeTableColumn<Subtask, String> tagsColumn;

    @FXML
    private TreeTableColumn<Subtask, Boolean> statusColumn;

    @FXML
    private Button btnAddTask;

    @FXML
    private Button btnUpdTask;

    @FXML
    private Button btnToFilter;

    @FXML
    private Button btnAddSubtask;

    @FXML
    private Button btnUpSubtask;

    @FXML
    private Button btnDownSubtask;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnComplete;

    @FXML
    private Spinner spinTask;

    @FXML
    private CheckBox chxShowSubtasks;

    // Вкладка фильтрации
    @FXML
    private TreeTableView<Subtask> listTasksTreeTableViewF;

    @FXML
    private TreeTableColumn<Subtask, String> nameColumnF;

    @FXML
    private TreeTableColumn<Subtask, String> descriptionColumnF;

    @FXML
    private TreeTableColumn<Subtask, String> deadlineColumnF;

    @FXML
    private TreeTableColumn<Subtask, String> tagsColumnF;

    @FXML
    private TreeTableColumn<Subtask, Boolean> statusColumnF;

    @FXML
    private ComboBox<String> cbxFilters;

    @FXML
    private ComboBox<String> cbxTag;

    @FXML
    private TextField tfdSubstr;

    enum actFrm {INSERT, UPDATE};

    static TaskDao taskDao = null;

    @FXML
    void initialize() {
        // Первая вкладка
        listTasksTreeTableView.setOnMouseReleased(event -> changeEnableButton());
        listTasksTreeTableView.setOnKeyReleased(event -> changeEnableButton());
        cbxTag.setOnAction(event -> printFilterLists());
        btnAddTask.setOnAction(event -> addTask());
        btnDelete.setOnAction(event -> delTaskOrSubtask());
        btnUpdTask.setOnAction(event -> updTaskOrSubtask());
        btnToFilter.setOnAction(event -> tabPane.getSelectionModel().select(tabFilter));
        btnAddSubtask.setOnAction(event -> addSubtask());
        btnComplete.setOnAction(event -> completeTaskOrSubtask());
        btnUpSubtask.setOnAction(event -> changeOrderSubtask());
        btnDownSubtask.setOnAction(event -> changeOrderSubtask());
        chxShowSubtasks.setOnAction(event -> printListOnTreeTable());

        // Вторая вкладка
        cbxTag.setVisible(false);
        tfdSubstr.setVisible(false);
        tabFilter.setOnSelectionChanged(event -> printFilterLists());
        tfdSubstr.setOnAction(event -> printFilterLists());
        tfdSubstr.setOnKeyReleased(event -> printFilterLists());

        cbxFilters.getItems().setAll(
                "Список незавершенных",
                "Список незавершенных просроченных + на ближайщую неделю",
                "Список незавершенных по тегу",
                "Список заверщенных",
                "Список задач на ближайший месяц + тег + подстрока",
                "Список незаврешнных задач, в которых по крайней мере половина подзадач завершена",
                "Просроченные + отмеченные хотябы одним из 3-х самых популярных тегов",
                "Три задачи с ближайшим крайним сроком, отмеченные выбираемым тегом",
                "Четыре задачи с самым отдалённым крайним сроком, не отмеченные никаким тегом");
        cbxFilters.getSelectionModel().select(0);
        cbxFilters.setOnAction(event -> printFilterLists());

        SpinnerValueFactory<Integer> spinnerValueFactory=new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,5);
        this.spinTask.setValueFactory(spinnerValueFactory);
        spinTask.setOnKeyReleased(event -> printListOnTreeTable());
        spinTask.setOnMouseReleased(event -> printListOnTreeTable());

        printListOnTreeTable();
    }

    // Добавление задачи
    private void addTask()  {
        createAndShowStageUpsertTask("Добавить задачу",null, actFrm.INSERT);
    }

    // Добавление подзадачи
    private void addSubtask()  {
        Task task = (Task) listTasksTreeTableView.getSelectionModel().getSelectedItem().getValue();
        createAndShowStageUpsertSubask("Добавить подзадачу", task, -1, actFrm.INSERT);
    }

    // Изменение задачи/подзадачи
    private void updTaskOrSubtask() {
        TreeItem <Subtask> selectedItem = listTasksTreeTableView.getSelectionModel().getSelectedItem();
        TreeItem <Subtask> parentItem = selectedItem.getParent();
        if (selectedItem.getValue() instanceof Task)
            createAndShowStageUpsertTask("Изменить задачу", (Task) selectedItem.getValue(), actFrm.UPDATE);
        else
            createAndShowStageUpsertSubask("Изменить подзадачу", (Task) parentItem.getValue(),
                    parentItem.getChildren().indexOf(selectedItem), actFrm.UPDATE);
    }

    // Завершение задачи/подзадачи
    private void completeTaskOrSubtask() {
        TreeItem <Subtask> selectedItem = listTasksTreeTableView.getSelectionModel().getSelectedItem();
        TreeItem <Subtask> parentItem = selectedItem.getParent();
        if (selectedItem.getValue() instanceof Task) {
            Task task = (Task) selectedItem.getValue();
            task.setCompleted();
            taskDao.update(task);
        }
        else {
            Task task = (Task) parentItem.getValue();
            int indSubtask = parentItem.getChildren().indexOf(selectedItem);
            task.getSubtasks().get(indSubtask).setCompleted();
            taskDao.update(task);
        }
        printListOnTreeTable();
    }

    // Удаление задачи/подзадачи
    private void delTaskOrSubtask() {
        TreeItem<Subtask> selectedItem = listTasksTreeTableView.getSelectionModel().getSelectedItem();
        TreeItem<Subtask> parentItem = selectedItem.getParent();
        if (selectedItem.getValue() instanceof Task)
            taskDao.delete((Task) selectedItem.getValue());
        else {
            Task task = (Task) parentItem.getValue();
            int indSubtask = parentItem.getChildren().indexOf(selectedItem);
            task.getSubtasks().remove(indSubtask);
            taskDao.update(task);
        }
        printListOnTreeTable();
    }

    // Печать списков
    private void printFilterLists() {
        /*
              0.  "Список незавершенных",
              1.  "Список незавершенных просроченных + на ближайщую неделю",
              2.  "Список незавершенных по тегу",
              3.  "Список заверщенных",
              4.  "Список задач на ближайший месяц + тег + подстрока",
              5.  "Список незаврешнных задач, в которых по крайней мере половина подзадач завершена",
              6.  "Просроченные + отмеченные хотябы одним из 3-х самых популярных тегов",
              7.  "Три задачи с ближайшим крайним сроком, отмеченные выбираемым тегом",
              8.  "Четыре задачи с самым отдалённым крайним сроком, не отмеченные никаким тегом"
        */

        int countTaskView = 50;
        cbxTag.getItems().setAll(taskDao.getAll(countTaskView,0).stream()
                .flatMap(x -> x.getTags().stream().filter(y -> !y.isEmpty())).distinct().collect(Collectors.toList()));
        int indFilter = cbxFilters.getSelectionModel().getSelectedIndex();
        cbxTag.setVisible(indFilter == 2 || indFilter == 4 || indFilter == 7);
        tfdSubstr.setVisible(indFilter == 4);
        switch (indFilter) {
            case 0:
                printFilterList(taskDao.getAllNotComleted());
                break;
            case 1:
                printFilterList(taskDao.getAllNotComletedOverdueAndForWeek());
                break;
            case 2:
                Optional<ArrayList> list = Optional.ofNullable(taskDao.getAllNotCompletedWithTag(cbxTag.getValue().trim()));
                printFilterList(list.orElseGet(ArrayList::new));
                break;
            case 3:
                printFilterList(taskDao.getAllCompleted());
                break;
            case 4:
                if (tfdSubstr.getText().trim().isEmpty() || cbxTag.getValue().isEmpty())
                    printFilterList(new ArrayList <>());
                else
                    printFilterList(taskDao.getAllWithTagAndSubstrInDescription(cbxTag.getValue(), tfdSubstr.getText().trim()));
                break;
            case 5:
                printFilterList(taskDao.getAllNotComletedWhereHalfNotCompleted());
                break;
            case 6:
                printFilterList(taskDao.getAllNotComletedOverdueAndWithPopularTag());
                break;
            case 7:
                Optional<ArrayList> list1 = Optional.ofNullable(taskDao.getThreeTasksWithNearestDeadlineWithTag(cbxTag.getValue().trim()));
                printFilterList(list1.orElseGet(ArrayList::new));
                break;
            case 8:
                printFilterList(taskDao.getFourTasksWithMostDistantDeadlineWithoutTags());
                break;
        }
    }

    // Создание и запуск формыы добавления/изменения подзадачи
    private void createAndShowStageUpsertSubask(String title, Task task, int indSubtask, actFrm act) {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/upsertSubtaskFrm.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage upsertSubtaskFrm = new Stage();
        upsertSubtaskFrm.initModality(Modality.APPLICATION_MODAL);
        upsertSubtaskFrm.setResizable(false);
        upsertSubtaskFrm.setTitle(title);
        upsertSubtaskFrm.setScene(new Scene(root, 320, 190));
        loader. <UpsertSubtaskFrm>getController().initialize(act, task, indSubtask);
        upsertSubtaskFrm.setOnHidden(event -> printListOnTreeTable());
        upsertSubtaskFrm.setOnCloseRequest(event -> printListOnTreeTable());
        upsertSubtaskFrm.show();
    }

    // Создание и запуск формыы добавления/изменения задачи
    private void createAndShowStageUpsertTask(String title, Task task, actFrm act) {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/upsertTaskFrm.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage upsertFrm = new Stage();
        upsertFrm.initModality(Modality.APPLICATION_MODAL);
        upsertFrm.setResizable(false);
        upsertFrm.setTitle(title);
        upsertFrm.setScene(new Scene(root, 320, 300));

        loader. <UpsertTaskFrm>getController().initialize(act, task);
        upsertFrm.setOnHidden(event -> printListOnTreeTable());
        upsertFrm.setOnCloseRequest(event -> printListOnTreeTable());
        upsertFrm.show();
    }

    // Печать текущего списка
    private void printListOnTreeTable(){
        btnUpdTask.setDisable(true);
        btnDelete.setDisable(true);
        btnComplete.setDisable(true);
        btnAddSubtask.setDisable(true);
        btnUpSubtask.setVisible(false);
        btnDownSubtask.setVisible(false);
        btnAddSubtask.setVisible(true);
        printTreeTable(taskDao.getAll((Integer)spinTask.getValue(), 0), listTasksTreeTableView, nameColumn, descriptionColumn, deadlineColumn, tagsColumn, statusColumn);
    }

    // Печать фильтрованных списков
    private void printFilterList(ArrayList<Task> tasks){
        printTreeTable(tasks, listTasksTreeTableViewF, nameColumnF, descriptionColumnF, deadlineColumnF, tagsColumnF, statusColumnF);
    }

    // Вывод дерева
    private void printTreeTable(ArrayList<Task> tasks, TreeTableView <Subtask> listTasksTreeTableView,
                                TreeTableColumn <Subtask, String> nameColumn, TreeTableColumn <Subtask, String> descriptionColumn,
                                TreeTableColumn <Subtask, String> deadlineColumn, TreeTableColumn <Subtask, String> tagsColumn,
                                TreeTableColumn <Subtask, Boolean> statusColumn) {
        TreeItem <Subtask> root = new TreeItem <>(null);
        listTasksTreeTableView.setRoot(root);
        listTasksTreeTableView.setShowRoot(false);

        for (Task task : tasks) {
            TreeItem <Subtask> taskTreeItem = new TreeItem <>(task);
            for (Subtask subtaskTreeItem : task.getSubtasks())
                taskTreeItem.getChildren().add(new TreeItem <>(subtaskTreeItem));
            taskTreeItem.setExpanded(chxShowSubtasks.isSelected());
            root.getChildren().add(taskTreeItem);
        }

        // Заносим значение в столбец "название"
        nameColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getParent().getValue() == null
                ? "  " + param.getValue().getValue().getName()          // Задача с маленьким отступом
                : "     " + param.getValue().getValue().getName()));    // Подзачача с большим отступом

        // Заносим значение в столбец "описание"
        descriptionColumn.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getValue().getDescription()));


        // Заносим значение в столбец "дедлайн"
        deadlineColumn.setCellValueFactory(param -> of(new SimpleStringProperty(
                param.getValue().getValue() instanceof Task && ofNullable(((Task) param.getValue().getValue()).getDeadline()).isPresent()
                        ? ((Task) param.getValue().getValue()).getDeadline().format(DateTimeFormatter.ofPattern("dd.MM.yyyy (E)")) : ""))
                        .orElse(new SimpleStringProperty("")));

        // Заносим значение в столбец "теги"
        tagsColumn.setCellValueFactory(param ->
                of(new SimpleStringProperty(param.getValue().getValue() instanceof Task
                        ? ((Task) param.getValue().getValue()).getTags()
                        .stream().filter(x -> !x.trim().isEmpty())
                        .map(x -> " #" + x).reduce((x, y) -> x + y).orElse("") : "")).orElse(new SimpleStringProperty("")));

        // Делаем ячейку для
        statusColumn.setCellFactory(CheckBoxTreeTableCell.forTreeTableColumn(statusColumn));
        statusColumn.setCellValueFactory(param -> new SimpleBooleanProperty(param.getValue().getValue().isCompleted()));
    }

    /// Другие обработчики
    private void changeOrderSubtask(){
        TreeItem<Subtask> si = listTasksTreeTableView.getSelectionModel().getSelectedItem();
        Optional<Subtask> st = Optional.ofNullable(si.getParent().getValue());
        if (!st.isPresent()) return;
        Task task = (Task) st.get();
        int indSubtask = task.getSubtasks().indexOf(si.getValue());
        if  (btnUpSubtask.isFocused())
            swapSubtask(task, indSubtask, indSubtask - 1);
        else
            swapSubtask(task, indSubtask, indSubtask + 1);
        taskDao.update(task);
        printListOnTreeTable();
    }

    private void swapSubtask(Task task, int indSubtask, int i) {
        Subtask temp = task.getSubtasks().get(indSubtask);
        task.getSubtasks().set(indSubtask, task.getSubtasks().get(i));
        task.getSubtasks().set(i, temp);
    }

    private void changeEnableButton() {
        Optional <TreeItem <Subtask>> si = Optional.ofNullable(listTasksTreeTableView.getSelectionModel().getSelectedItem());
        if (!si.isPresent()) return;
        Subtask subtask = si.get().getValue();
        if (subtask instanceof Task) {
            btnAddSubtask.setVisible(true);
            btnUpSubtask.setVisible(false);
            btnDownSubtask.setVisible(false);
            btnAddSubtask.setDisable(false);
        } else {
            btnAddSubtask.setVisible(false);
            btnDownSubtask.setVisible(true);
            btnUpSubtask.setVisible(true);
            int ind = listTasksTreeTableView.getSelectionModel().getSelectedItem().getParent().getChildren()
                    .indexOf(listTasksTreeTableView.getSelectionModel().getSelectedItem());
            int count = listTasksTreeTableView.getSelectionModel().getSelectedItem().getParent().getChildren().size() - 1;
            if (ind == 0 && ind == count) {
                btnDownSubtask.setDisable(true);
                btnUpSubtask.setDisable(true);
            } else if (ind == 0) {
                btnDownSubtask.setDisable(false);
                btnUpSubtask.setDisable(true);
            } else if (ind == count) {
                btnDownSubtask.setDisable(true);
                btnUpSubtask.setDisable(false);
            } else {
                btnDownSubtask.setDisable(false);
                btnUpSubtask.setDisable(false);
            }
        }
        btnComplete.setDisable(subtask.isCompleted());
        btnDelete.setDisable(false);
        btnUpdTask.setDisable(false);
    }
}
