package lab4.java.frontend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lab4.java.common.TaskDao;

public class GUI extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/mainFrm.fxml"));
        primaryStage.setTitle("Задачник");
        primaryStage.setScene(new Scene(root, 930, 400));
        primaryStage.show();
        primaryStage.setResizable(false);

    }


    public void start(TaskDao td) {
        MainFrm.taskDao = td;
        launch();
    }
}
