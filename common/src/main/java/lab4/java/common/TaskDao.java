package lab4.java.common;

import java.util.ArrayList;

public interface TaskDao {
    // Создание/загрузка из файла списка задач
    void create();

    // Сохранение списка задач в файл
    void save();

    // Добавление задачи
    void insert(Task task);

    // Изменение задачи
    void update(Task task);

    // Удаление задачи
    void delete(Task task);

    // Получить список всех задач
    ArrayList<Task> getAll(long limit, long offset);

    /// ФИЛЬТРАЦИЯ ///

    // Получить список всех незаверешнных задач, отсортированных по названию по возрастанию
    ArrayList<Task> getAllNotComleted();

    // Получить список всех незавершенных задач, отмеченных выбранным тегом, отсортированных по названию по возрастанию
    ArrayList<Task> getAllNotCompletedWithTag(String tag);

    // Получить список всех незавершенных просроченных задач и задач со сроком выполнения в ближайшую неделю,
    // отсортированных по сроку выполнения по возрастанию, по названию по возрастанию
    ArrayList<Task> getAllNotComletedOverdueAndForWeek();

    // Получить список всех завершенных задач, отсортированных по возрастснию
    ArrayList<Task> getAllCompleted();

    //Задачи на ближайший месяц, помеченные выбираемым тегом и содержащие в тексте описания указываемую подстроку.
    ArrayList<Task> getAllWithTagAndSubstrInDescription(String tag, String substr);

    //Незавершённые задачи, в которых по крайней мере половина подзадач завершена.
    ArrayList<Task> getAllNotComletedWhereHalfNotCompleted();

    //Просроченные задачи, отмеченные по крайней мере одним из
    // трёх самых часто используемых тегов среди всех задач
    ArrayList<Task> getAllNotComletedOverdueAndWithPopularTag();

    // Три задачи с ближайшим крайним сроком, отмеченные выбираемым тегом.
    ArrayList<Task> getThreeTasksWithNearestDeadlineWithTag(String tag);

    // Четыре задачи с самым отдалённым крайним сроком, не отмеченные никаким тегом
    ArrayList<Task> getFourTasksWithMostDistantDeadlineWithoutTags();
}
